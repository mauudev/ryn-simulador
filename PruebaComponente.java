
/**
 * Write a description of class PruebaComponente here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 * lunes a viernes de 08:30 a 20:30 hs.
 * los sábados de 8:00 a 13:00. 
 * 
 * 25 de cada uno de lunes a viernes
 * 22 de cada uno los sabados
 * 
 * 25 de cada uno al dia de lunes a viernes, 50 c/u, 150 total
 * 21 de cada uno al dia los sabados, 105 c/u, total 315 al dia
 * 
 * en total a la semana el stock es 150*5 + 315 = 1065
 * 1065/3 = 355 de cada uno a la semana entran a deposito para pruebas
 * 
 */
import java.util.*;
import java.text.DecimalFormat;
public class PruebaComponente extends Thread{
    DistribucionNormal dn = new DistribucionNormal();
    int depositoCPU_entrada = 0;
    int depositoMonitores_entrada = 0;
    int depositoTeclados_entrada = 0;
    int aux = 0;
    
    int depositoCPU_fallas = 0;
    int depositoMonitores_fallas = 0;
    int depositoTeclados_fallas = 0;
    
    int depositoCPU_empaquetado = 0;
    int depositoMonitores_empaquetado = 0;
    int depositoTeclados_empaquetado = 0;
    
    String[] dias = {"Lunes","Martes","Miercoles","Jueves","Viernes","Sabado"};
    
    int cantCPU_lv, cantMonitores_lv, cantTeclados_lv = 50;
    int cantCPU_s, cantMonitores_s, cantTeclados_s = 105;
    
    int minsLaborales_lv = 720;//12 horas
    int minsLaborales_s = 300;//5 horas
    
    //variables para determinar cuantos componentes se prueban al dia y sus minutos correspondientes
    int cantidadCPU_prueba = 20;
    int cantidadMonitores_prueba = 0;
    int cantidadTeclados_prueba = 36;
    int minsCPU_prueba = 0;
    int minsMonitores_prueba = 0;
    int minsTeclados_prueba =0;
    ArrayList<Integer> minsMonitores = new ArrayList<Integer>();
    
    int tiempoPrueba_CPU = 35;//uniforme
    //normal
    int tiempoPrueba_teclado = 20;//uniforme
     
    int minsTranscurridos = 0;
           
    @Override
    public void run(){
      for(int i = 0; i < dias.length; i++){
          switch(dias[i]){
            case "Lunes":
                System.out.println("Dia: "+dias[i]);
                sleep(2);
                System.out.println("******** INICIO PRUEBA CPU ********");
                pruebaCPU();
                System.out.println("******** FIN PRUEBA CPU ********");
                sleep(2);
                System.out.println("******** INICIO PRUEBA MONITORES ********");
                pruebaMonitores();
                System.out.println("******** FIN PRUEBA MONITORES ********");
                sleep(2);            
                System.out.println("******** INICIO PRUEBA TECLADOS ********");
                pruebaTeclados();
                System.out.println("******** FIN PRUEBA TECLADOS ********");
                sleep(2);
                
                System.out.println("RESULTADOS:");
                System.out.println("Cantidad de CPUs en deposito de empaque: "+depositoCPU_empaquetado);
                System.out.println("Cantidad de CPU en deposito de reparaciones: "+depositoCPU_fallas);
                System.out.println("Cantidad de CPU en deposito de entrada para el dia siguiente: "+depositoCPU_entrada);
                
                System.out.println("Cantidad de monitores en deposito de empaque: "+depositoMonitores_empaquetado);
                System.out.println("Cantidad de monitores en deposito de reparaciones: "+depositoMonitores_fallas);
                System.out.println("Cantidad de monitores en deposito de entrada para el dia siguiente: "+depositoMonitores_entrada);
                
                System.out.println("Cantidad de teclados en deposito de empaque: "+depositoTeclados_empaquetado);
                System.out.println("Cantidad de teclados en deposito de reparaciones: "+depositoTeclados_fallas);
                System.out.println("Cantidad de monitores en deposito de entrada para el dia siguiente: "+depositoTeclados_entrada);
                break;
            case "Martes":
                System.out.println("Dia: "+dias[i]);
                sleep(2);
                System.out.println("******** INICIO PRUEBA CPU ********");
                pruebaCPU();
                System.out.println("******** FIN PRUEBA CPU ********");
                sleep(2);
                System.out.println("******** INICIO PRUEBA MONITORES ********");
                pruebaMonitores();
                System.out.println("******** FIN PRUEBA MONITORES ********");
                sleep(2);            
                System.out.println("******** INICIO PRUEBA TECLADOS ********");
                pruebaTeclados();
                System.out.println("******** FIN PRUEBA TECLADOS ********");
                sleep(2);
                
                System.out.println("RESULTADOS:");
                System.out.println("Cantidad de CPUs en deposito de empaque: "+depositoCPU_empaquetado);
                System.out.println("Cantidad de CPU en deposito de reparaciones: "+depositoCPU_fallas);
                System.out.println("Cantidad de CPU en deposito de entrada para el dia siguiente: "+depositoCPU_entrada);
                
                System.out.println("Cantidad de monitores en deposito de empaque: "+depositoMonitores_empaquetado);
                System.out.println("Cantidad de monitores en deposito de reparaciones: "+depositoMonitores_fallas);
                System.out.println("Cantidad de monitores en deposito de entrada para el dia siguiente: "+depositoMonitores_entrada);
                
                System.out.println("Cantidad de teclados en deposito de empaque: "+depositoTeclados_empaquetado);
                System.out.println("Cantidad de teclados en deposito de reparaciones: "+depositoTeclados_fallas);
                System.out.println("Cantidad de monitores en deposito de entrada para el dia siguiente: "+depositoTeclados_entrada);
                break;
            case "Miercoles":
                System.out.println("Dia: "+dias[i]);
                sleep(2);
                System.out.println("******** INICIO PRUEBA CPU ********");
                pruebaCPU();
                System.out.println("******** FIN PRUEBA CPU ********");
                sleep(2);
                System.out.println("******** INICIO PRUEBA MONITORES ********");
                pruebaMonitores();
                System.out.println("******** FIN PRUEBA MONITORES ********");
                sleep(2);            
                System.out.println("******** INICIO PRUEBA TECLADOS ********");
                pruebaTeclados();
                System.out.println("******** FIN PRUEBA TECLADOS ********");
                sleep(2);
                
                System.out.println("RESULTADOS:");
                System.out.println("Cantidad de CPUs en deposito de empaque: "+depositoCPU_empaquetado);
                System.out.println("Cantidad de CPU en deposito de reparaciones: "+depositoCPU_fallas);
                System.out.println("Cantidad de CPU en deposito de entrada para el dia siguiente: "+depositoCPU_entrada);
                
                System.out.println("Cantidad de monitores en deposito de empaque: "+depositoMonitores_empaquetado);
                System.out.println("Cantidad de monitores en deposito de reparaciones: "+depositoMonitores_fallas);
                System.out.println("Cantidad de monitores en deposito de entrada para el dia siguiente: "+depositoMonitores_entrada);
                
                System.out.println("Cantidad de teclados en deposito de empaque: "+depositoTeclados_empaquetado);
                System.out.println("Cantidad de teclados en deposito de reparaciones: "+depositoTeclados_fallas);
                System.out.println("Cantidad de monitores en deposito de entrada para el dia siguiente: "+depositoTeclados_entrada);
                break;
            case "Jueves":
                System.out.println("Dia: "+dias[i]);
                sleep(2);
                System.out.println("******** INICIO PRUEBA CPU ********");
                pruebaCPU();
                System.out.println("******** FIN PRUEBA CPU ********");
                sleep(2);
                System.out.println("******** INICIO PRUEBA MONITORES ********");
                pruebaMonitores();
                System.out.println("******** FIN PRUEBA MONITORES ********");
                sleep(2);            
                System.out.println("******** INICIO PRUEBA TECLADOS ********");
                pruebaTeclados();
                System.out.println("******** FIN PRUEBA TECLADOS ********");
                sleep(2);
                
                System.out.println("RESULTADOS:");
                System.out.println("Cantidad de CPUs en deposito de empaque: "+depositoCPU_empaquetado);
                System.out.println("Cantidad de CPU en deposito de reparaciones: "+depositoCPU_fallas);
                System.out.println("Cantidad de CPU en deposito de entrada para el dia siguiente: "+depositoCPU_entrada);
                
                System.out.println("Cantidad de monitores en deposito de empaque: "+depositoMonitores_empaquetado);
                System.out.println("Cantidad de monitores en deposito de reparaciones: "+depositoMonitores_fallas);
                System.out.println("Cantidad de monitores en deposito de entrada para el dia siguiente: "+depositoMonitores_entrada);
                
                System.out.println("Cantidad de teclados en deposito de empaque: "+depositoTeclados_empaquetado);
                System.out.println("Cantidad de teclados en deposito de reparaciones: "+depositoTeclados_fallas);
                System.out.println("Cantidad de monitores en deposito de entrada para el dia siguiente: "+depositoTeclados_entrada);
                break;
            case "Viernes":
                System.out.println("Dia: "+dias[i]);
                sleep(2);
                System.out.println("******** INICIO PRUEBA CPU ********");
                pruebaCPU();
                System.out.println("******** FIN PRUEBA CPU ********");
                sleep(2);
                System.out.println("******** INICIO PRUEBA MONITORES ********");
                pruebaMonitores();
                System.out.println("******** FIN PRUEBA MONITORES ********");
                sleep(2);            
                System.out.println("******** INICIO PRUEBA TECLADOS ********");
                pruebaTeclados();
                System.out.println("******** FIN PRUEBA TECLADOS ********");
                sleep(2);
                
                System.out.println("RESULTADOS:");
                System.out.println("Cantidad de CPUs en deposito de empaque: "+depositoCPU_empaquetado);
                System.out.println("Cantidad de CPU en deposito de reparaciones: "+depositoCPU_fallas);
                System.out.println("Cantidad de CPU en deposito de entrada para el dia siguiente: "+depositoCPU_entrada);
                
                System.out.println("Cantidad de monitores en deposito de empaque: "+depositoMonitores_empaquetado);
                System.out.println("Cantidad de monitores en deposito de reparaciones: "+depositoMonitores_fallas);
                System.out.println("Cantidad de monitores en deposito de entrada para el dia siguiente: "+depositoMonitores_entrada);
                
                System.out.println("Cantidad de teclados en deposito de empaque: "+depositoTeclados_empaquetado);
                System.out.println("Cantidad de teclados en deposito de reparaciones: "+depositoTeclados_fallas);
                System.out.println("Cantidad de monitores en deposito de entrada para el dia siguiente: "+depositoTeclados_entrada);
                break;
            case "Sabado":
            break;
          }
      }
    }
    private void pruebaCPU(){//dura 35 mins/cpu
        System.out.println("Llegada de camion con 25 unidades de CPU's..");
        //sleep(1);
        depositoCPU_entrada += 50;
        int limite = cantidadCPU_prueba;  
        int cantCPU_falla = (50*8)/100;//8% -> 2
        System.out.println("Cantidad de CPUs en deposito: "+depositoCPU_entrada);
        System.out.println("Fase de prueba de CPUs..");
        for(int i = 1; i <= limite; i++){
            System.out.println("Prueba CPU: "+i);
            //sleep(1);
            if(cantCPU_falla != 0){
                if(falla()){
                    System.out.println("Prueba realizada: Con falla, se lleva a deposito de reparaciones.");
                    depositoCPU_fallas++;
                    cantCPU_falla--; 
                    minsTranscurridos += 35;
                    depositoCPU_entrada--;
                    System.out.println("Cantidad de CPU en deposito de empaque: "+depositoCPU_empaquetado);
                    System.out.println("Cantidad de CPU en deposito de reparaciones: "+depositoCPU_fallas);
                    System.out.println("Cantidad de CPU en deposito de entrada: "+depositoCPU_entrada);
                    System.out.println("Minutos transcurridos: "+minsTranscurridos+" minutos.");
                }else{
                    System.out.println("Prueba realizada: Sin falla, se lleva a deposito de empaque.");
                    depositoCPU_empaquetado++;
                    minsTranscurridos += 35;
                    depositoCPU_entrada--;
                    System.out.println("Cantidad de CPU en deposito de empaque: "+depositoCPU_empaquetado);
                    System.out.println("Cantidad de CPU en deposito de reparaciones: "+depositoCPU_fallas);
                    System.out.println("Cantidad de CPU en deposito de entrada: "+depositoCPU_entrada);
                    System.out.println("Minutos transcurridos: "+minsTranscurridos+" minutos.");
                }
            }else{
                    System.out.println("Prueba realizada: Sin falla, se lleva a deposito de empaque.");
                    depositoCPU_empaquetado++;
                    depositoCPU_entrada--;
                    minsTranscurridos += 35;
                    System.out.println("Cantidad de CPU en deposito de empaque: "+depositoCPU_empaquetado);
                    System.out.println("Cantidad de CPU en deposito de reparaciones: "+depositoCPU_fallas);
                    System.out.println("Cantidad de CPU en deposito de entrada: "+depositoCPU_entrada);
                    System.out.println("Minutos transcurridos: "+minsTranscurridos+" minutos.");
            }
        }
        minsTranscurridos = 0;//para la sgte prueba
    }
    private void pruebaTeclados(){//20 mins, 36 al dia
        System.out.println("Llegada de camion con 25 unidades de teclados..");
        //sleep(1);
        minsTeclados_prueba = getMinutosPruebaTeclados();
        depositoTeclados_entrada += 50;
        int cantTeclados_falla = (50*16)/100;//16% -> 4
        System.out.println("Cantidad de Teclados en deposito: "+depositoTeclados_entrada);
        System.out.println("Fase de prueba de teclados..");
        int limite = cantidadTeclados_prueba;
        for(int k = 1; k <= limite; k++){
            System.out.println("Prueba teclado: "+k);
            //sleep(1);
            if(cantTeclados_falla != 0){
                if(falla()){
                    System.out.println("Prueba realizada: Con falla, se lleva a deposito de reparaciones.");
                    depositoTeclados_fallas++;
                    cantTeclados_falla--;  
                    minsTranscurridos += 20;
                    depositoTeclados_entrada--;
                    System.out.println("Cantidad de teclados en deposito de empaque: "+depositoTeclados_empaquetado);
                    System.out.println("Cantidad de teclados en deposito de reparaciones: "+depositoTeclados_fallas);
                    System.out.println("Cantidad de teclados en deposito de entrada: "+depositoTeclados_entrada);
                    System.out.println("Minutos transcurridos: "+minsTranscurridos+" minutos."); 
                }else{
                    System.out.println("Prueba realizada: Sin falla, se lleva a deposito de empaque.");
                    depositoTeclados_empaquetado++;
                    minsTranscurridos += 20;
                    depositoTeclados_entrada--;
                    System.out.println("Cantidad de teclados en deposito de empaque: "+depositoTeclados_empaquetado);
                    System.out.println("Cantidad de teclados en deposito de reparaciones: "+depositoTeclados_fallas);
                    System.out.println("Cantidad de teclados en deposito de entrada: "+depositoTeclados_entrada);
                    System.out.println("Minutos transcurridos: "+minsTranscurridos+" minutos.");
                }
            }else{
                    System.out.println("Prueba realizada: Sin falla, se lleva a deposito de empaque.");
                    depositoTeclados_empaquetado++;
                    depositoTeclados_entrada--;
                    minsTranscurridos += 20;
                    System.out.println("Cantidad de teclados en deposito de empaque: "+depositoTeclados_empaquetado);
                    System.out.println("Cantidad de teclados en deposito de reparaciones: "+depositoTeclados_fallas);
                    System.out.println("Cantidad de teclados en deposito de entrada: "+depositoTeclados_entrada);
                    System.out.println("Minutos transcurridos: "+minsTranscurridos+" minutos.");
            }
       }
       minsTranscurridos = 0;//para la sgte prueba
    }
    private void pruebaMonitores(){
        System.out.println("Llegada de camion con 25 unidades de monitores..");
        depositoMonitores_entrada += 50;
        System.out.println("Cantidad de Monitores en deposito: "+depositoMonitores_entrada);
        System.out.println("Fase de prueba de monitores..");
        minsMonitores.clear();
        cantidadMonitores_prueba = 0;
        int minutosMonitores_prueba = getMinutosPruebaMonitores();
        int limite = cantidadMonitores_prueba;
        System.out.println("Limite: "+limite);
        int cantMonitores_falla = (50*12)/100;//12% -> 3
        System.out.println("limite: "+limite);
        System.out.println("size: "+minsMonitores.size());
        for(int j = 0; j < limite; j++){
            System.out.println("Prueba monitor: "+(j+1));
            //sleep(1);
            if(cantMonitores_falla != 0){
                if(falla()){
                    System.out.println("Prueba realizada: Con falla, se lleva a deposito de reparaciones.");
                    depositoMonitores_fallas++;
                    cantMonitores_falla--;   
                    minsTranscurridos += minsMonitores.get(j);
                    depositoMonitores_entrada--;
                    System.out.println("Cantidad de monitores en deposito de empaque: "+depositoMonitores_empaquetado);
                    System.out.println("Cantidad de monitores en deposito de reparaciones: "+depositoMonitores_fallas);
                    System.out.println("Minutos transcurridos: "+minsTranscurridos+" minutos.");
                }else{
                    System.out.println("Prueba realizada: Sin falla, se lleva a deposito de empaque.");
                    depositoMonitores_empaquetado++;
                    minsTranscurridos += minsMonitores.get(j);
                    depositoMonitores_entrada--;
                    System.out.println("Cantidad de monitores en deposito de empaque: "+depositoMonitores_empaquetado);
                    System.out.println("Cantidad de monitores en deposito de reparaciones: "+depositoMonitores_fallas);
                    System.out.println("Cantidad de monitores en deposito de entrada: "+depositoMonitores_entrada);
                    System.out.println("Minutos transcurridos: "+minsTranscurridos+" minutos.");
                }
            }else{
                    System.out.println("Prueba realizada: Sin falla, se lleva a deposito de empaque.");
                    depositoMonitores_empaquetado++;
                    depositoMonitores_entrada--;
                    minsTranscurridos += minsMonitores.get(j);
                    System.out.println("Cantidad de monitores en deposito de empaque: "+depositoMonitores_empaquetado);
                    System.out.println("Cantidad de monitores en deposito de reparaciones: "+depositoMonitores_fallas);
                    System.out.println("Cantidad de monitores en deposito de entrada: "+depositoMonitores_entrada);
                    System.out.println("Minutos transcurridos: "+minsTranscurridos+" minutos.");
            }
       }
       minsTranscurridos = 0;//para la sgte prueba
    }
    private void sleep(int segundos) {
        try {
            Thread.sleep(segundos * 1000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }
    public boolean falla(){
        Random rnd = new Random();
        int num = rnd.nextInt(2);
        if(num == 0) return false;
        else return true;
    }
    public int getMinutosPrueba_CPU(){//35 mins; se puede probar 20 CPU al dia (se gastan los 720 mins exactos al dia)
        return 20*35;
    }
    public int getMinutosPruebaMonitores(){//aleatorio !!
        int res = 0;
        boolean flag = true;
        int actual = 0;
        while(flag){
            if(res < 720){
                int aleatorio = dn.TCLNormal(30,7);
                minsMonitores.add(aleatorio);
                actual = aleatorio;
                res += aleatorio;
                cantidadMonitores_prueba ++;
            }else{
                res -= actual;
                cantidadMonitores_prueba --;
                minsMonitores.remove(minsMonitores.size()-1);
                flag = false;
            }
        }
        return res;
    }
    public int getCantidadMonitores_prueba(){return cantidadMonitores_prueba;}
    public int getArraylistSize(){return minsMonitores.size(); }
    public int getMinutosPruebaTeclados(){//20 mins; se pueden probar 36 teclados al dia (se gasta los 720 mins exactos)
        return 20*36;
    }
    public void pruebaMonitor(){
        for(int i =0; i<= 50; i++)
        System.out.println(getMinutosPruebaMonitores());
    }
        public int getMinsLaborales_lv(){
        return this.minsLaborales_lv;
    }
    public void restarMinsLaborales_lv(int mins){
        this.minsLaborales_lv -= mins; 
    }
    public int getMinsLaborales_s(){
        return this.minsLaborales_s;
    }
    public void restarMinsLaborales_s(int mins){
        this.minsLaborales_s -= mins; 
    }
}
