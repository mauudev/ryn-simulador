
/**
 * Write a description of class DistribucionNormal here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.*;
public class DistribucionNormal{
  	public int TCLNormal(float mu, float sigma){
		float t = 0;
		for(int i = 0;i < 12;i++){
			t += Math.random();//distribuye aprox. N(6,1)
		}
		t = (t - 6); //distribuye aprox. N(0,1)
		t = t * sigma + mu;//distribuye N(mu,sigma)
		return Math.round(t);
	}
}
